package com.neo.titulos.model;


import com.neo.titulos.utils.MessageUtils;

/**
 * Created by IlseParra on 19/11/2015.
 */
public class BirthCertificateBuilder {
    private String content;

    public BirthCertificateBuilder(String content) {
        super();
        this.content = content;
    }

    public BirthCertificateMessage getBirthCertificateMessage() {
        if (content != null) {
            BirthCertificateMessage message = new BirthCertificateMessage();
            message.setFcn(MessageUtils.getField(0, 8, content));
            message.setTcn(MessageUtils.getField(8, 15, content));
            message.setIcn(MessageUtils.getField(16, 32, content));
            message.setName(MessageUtils.getField(32, 80, content));
            message.setFirstSurname(MessageUtils.getField(80, 128, content));
            message.setSecondSurname(MessageUtils.getField(128, 176, content));
            message.setCrn(MessageUtils.getField(176, 224, content));
            message.setGender(MessageUtils.getField(224, 225, content));
            message.setBirthDate(MessageUtils.getField(225, 233, content));
            message.setBirthTime(MessageUtils.getField(233, 238, content));
            message.setIssuanceDate(MessageUtils.getField(240, 248, content));
            message.setIdDocument(MessageUtils.getField(256, 272, content));
            message.setBirthPlace(MessageUtils.getField(272, 320, content));
            message.setCity(MessageUtils.getField(320, 368, content));
            message.setStreet(MessageUtils.getField(368, 408, content));
            message.setNumber(MessageUtils.getField(408, 416, content));
            message.setBirthNameFirstParent(MessageUtils.getField(416, 464, content));
            message.setNameFirstParent(MessageUtils.getField(464, 512, content));
            message.setFirstSurnameFirstParent(MessageUtils.getField(512, 560, content));
            message.setSecondSurnameFirstParent(MessageUtils.getField(560, 608, content));
            message.setOccupationFirstParent("");
            message.setIdentificationCardFirstParent(MessageUtils.getField(640, 672, content));
            message.setCredentialNumberFirstParent(MessageUtils.getField(672, 704, content));
            message.setCitizenshipFirstParent(MessageUtils.getField(704, 736, content));
            message.setGenderFirstParent(MessageUtils.getField(736, 737, content));
            message.setBirthDateFirstParent(MessageUtils.getField(737, 745, content));
            message.setCitizenStatusFirstParent(MessageUtils.getField(752, 768, content));
            message.setBirthNameSecondParent(MessageUtils.getField(768, 816, content));
            message.setNameSecondParent(MessageUtils.getField(816, 864, content));
            message.setFirstSurnameSecondParent(MessageUtils.getField(864, 912, content));
            message.setSecondSurnameSecondParent(MessageUtils.getField(912, 960, content));
            message.setOccupationSecondParent("");
            message.setIdentificationCardSecondParent(MessageUtils.getField(992, 1024, content));
            message.setCredentialNumberSecondParent(MessageUtils.getField(1024, 1056, content));
            message.setCitizenshipSecondParent(MessageUtils.getField(1056, 1088, content));
            message.setGenderSecondParent(MessageUtils.getField(1088, 1089, content));
            message.setBirthDateSecondParent(MessageUtils.getField(1089, 1097, content));
            message.setCitizenStatusSecondParent(MessageUtils.getField(1104, 1120, content));
            message.setOfficerName(MessageUtils.getField(1120, 1184, content));
            message.setIssuancePlace(MessageUtils.getField(1184, 1232, content));
            message.setIssuingAuthority(MessageUtils.getField(1232, 1280, content));
            message.setIssuanceCountry(MessageUtils.getField(1280, 1328, content));

            return message;
        } else {
            throw new IllegalStateException("content must not be null");
        }
    }

}
