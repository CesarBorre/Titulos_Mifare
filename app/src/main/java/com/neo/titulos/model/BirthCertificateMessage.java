package com.neo.titulos.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by IlseParra on 19/11/2015.
 */
public class BirthCertificateMessage implements Parcelable {
    private String fcn;
    private String tcn;
    private String icn;
    private String name;
    private String firstSurname;
    private String secondSurname;
    private String crn;
    private String gender;
    private String birthDate;
    private String birthTime;
    private String issuanceDate;
    private String idDocument;

    private String birthPlace;
    private String city;
    private String street;
    private String number;

    private String birthNameFirstParent;
    private String nameFirstParent;
    private String firstSurnameFirstParent;
    private String secondSurnameFirstParent;
    private String occupationFirstParent;
    private String identificationCardFirstParent;
    private String credentialNumberFirstParent;
    private String citizenshipFirstParent;
    private String genderFirstParent;
    private String birthDateFirstParent;
    private String citizenStatusFirstParent;

    private String birthNameSecondParent;
    private String nameSecondParent;
    private String firstSurnameSecondParent;
    private String secondSurnameSecondParent;
    private String occupationSecondParent;
    private String identificationCardSecondParent;
    private String credentialNumberSecondParent;
    private String citizenshipSecondParent;
    private String genderSecondParent;
    private String birthDateSecondParent;
    private String citizenStatusSecondParent;

    private String officerName;
    private String issuancePlace;
    private String issuingAuthority;
    private String issuanceCountry;

    public BirthCertificateMessage() {
    }

    protected BirthCertificateMessage(Parcel in) {
        fcn = in.readString();
        tcn = in.readString();
        icn = in.readString();
        name = in.readString();
        firstSurname = in.readString();
        secondSurname = in.readString();
        crn = in.readString();
        gender = in.readString();
        birthDate = in.readString();
        birthTime = in.readString();
        issuanceDate = in.readString();
        idDocument = in.readString();
        birthPlace = in.readString();
        city = in.readString();
        street = in.readString();
        number = in.readString();
        birthNameFirstParent = in.readString();
        nameFirstParent = in.readString();
        firstSurnameFirstParent = in.readString();
        secondSurnameFirstParent = in.readString();
        occupationFirstParent = in.readString();
        identificationCardFirstParent = in.readString();
        credentialNumberFirstParent = in.readString();
        citizenshipFirstParent = in.readString();
        genderFirstParent = in.readString();
        birthDateFirstParent = in.readString();
        citizenStatusFirstParent = in.readString();
        birthNameSecondParent = in.readString();
        nameSecondParent = in.readString();
        firstSurnameSecondParent = in.readString();
        secondSurnameSecondParent = in.readString();
        occupationSecondParent = in.readString();
        identificationCardSecondParent = in.readString();
        credentialNumberSecondParent = in.readString();
        citizenshipSecondParent = in.readString();
        genderSecondParent = in.readString();
        birthDateSecondParent = in.readString();
        citizenStatusSecondParent = in.readString();
        officerName = in.readString();
        issuancePlace = in.readString();
        issuingAuthority = in.readString();
        issuanceCountry = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(fcn);
        dest.writeString(tcn);
        dest.writeString(icn);
        dest.writeString(name);
        dest.writeString(firstSurname);
        dest.writeString(secondSurname);
        dest.writeString(crn);
        dest.writeString(gender);
        dest.writeString(birthDate);
        dest.writeString(birthTime);
        dest.writeString(issuanceDate);
        dest.writeString(idDocument);
        dest.writeString(birthPlace);
        dest.writeString(city);
        dest.writeString(street);
        dest.writeString(number);
        dest.writeString(birthNameFirstParent);
        dest.writeString(nameFirstParent);
        dest.writeString(firstSurnameFirstParent);
        dest.writeString(secondSurnameFirstParent);
        dest.writeString(occupationFirstParent);
        dest.writeString(identificationCardFirstParent);
        dest.writeString(credentialNumberFirstParent);
        dest.writeString(citizenshipFirstParent);
        dest.writeString(genderFirstParent);
        dest.writeString(birthDateFirstParent);
        dest.writeString(citizenStatusFirstParent);
        dest.writeString(birthNameSecondParent);
        dest.writeString(nameSecondParent);
        dest.writeString(firstSurnameSecondParent);
        dest.writeString(secondSurnameSecondParent);
        dest.writeString(occupationSecondParent);
        dest.writeString(identificationCardSecondParent);
        dest.writeString(credentialNumberSecondParent);
        dest.writeString(citizenshipSecondParent);
        dest.writeString(genderSecondParent);
        dest.writeString(birthDateSecondParent);
        dest.writeString(citizenStatusSecondParent);
        dest.writeString(officerName);
        dest.writeString(issuancePlace);
        dest.writeString(issuingAuthority);
        dest.writeString(issuanceCountry);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BirthCertificateMessage> CREATOR = new Creator<BirthCertificateMessage>() {
        @Override
        public BirthCertificateMessage createFromParcel(Parcel in) {
            return new BirthCertificateMessage(in);
        }

        @Override
        public BirthCertificateMessage[] newArray(int size) {
            return new BirthCertificateMessage[size];
        }
    };

    public String getFcn() {
        return fcn;
    }

    public void setFcn(String fcn) {
        this.fcn = fcn;
    }

    public String getTcn() {
        return tcn;
    }

    public void setTcn(String tcn) {
        this.tcn = tcn;
    }

    public String getIcn() {
        return icn;
    }

    public void setIcn(String icn) {
        this.icn = icn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstSurname() {
        return firstSurname;
    }

    public void setFirstSurname(String firstSurname) {
        this.firstSurname = firstSurname;
    }

    public String getSecondSurname() {
        return secondSurname;
    }

    public void setSecondSurname(String secondSurname) {
        this.secondSurname = secondSurname;
    }

    public String getCrn() {
        return crn;
    }

    public void setCrn(String crn) {
        this.crn = crn;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getBirthTime() {
        return birthTime;
    }

    public void setBirthTime(String birthTime) {
        this.birthTime = birthTime;
    }

    public String getIssuanceDate() {
        return issuanceDate;
    }

    public void setIssuanceDate(String issuanceDate) {
        this.issuanceDate = issuanceDate;
    }

    public String getIdDocument() {
        return idDocument;
    }

    public void setIdDocument(String idDocument) {
        this.idDocument = idDocument;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getBirthNameFirstParent() {
        return birthNameFirstParent;
    }

    public void setBirthNameFirstParent(String birthNameFirstParent) {
        this.birthNameFirstParent = birthNameFirstParent;
    }

    public String getNameFirstParent() {
        return nameFirstParent;
    }

    public void setNameFirstParent(String nameFirstParent) {
        this.nameFirstParent = nameFirstParent;
    }

    public String getFirstSurnameFirstParent() {
        return firstSurnameFirstParent;
    }

    public void setFirstSurnameFirstParent(String firstSurnameFirstParent) {
        this.firstSurnameFirstParent = firstSurnameFirstParent;
    }

    public String getSecondSurnameFirstParent() {
        return secondSurnameFirstParent;
    }

    public void setSecondSurnameFirstParent(String secondSurnameFirstParent) {
        this.secondSurnameFirstParent = secondSurnameFirstParent;
    }

    public String getOccupationFirstParent() {
        return occupationFirstParent;
    }

    public void setOccupationFirstParent(String occupationFirstParent) {
        this.occupationFirstParent = occupationFirstParent;
    }

    public String getIdentificationCardFirstParent() {
        return identificationCardFirstParent;
    }

    public void setIdentificationCardFirstParent(String identificationCardFirstParent) {
        this.identificationCardFirstParent = identificationCardFirstParent;
    }

    public String getCredentialNumberFirstParent() {
        return credentialNumberFirstParent;
    }

    public void setCredentialNumberFirstParent(String credentialNumberFirstParent) {
        this.credentialNumberFirstParent = credentialNumberFirstParent;
    }

    public String getCitizenshipFirstParent() {
        return citizenshipFirstParent;
    }

    public void setCitizenshipFirstParent(String citizenshipFirstParent) {
        this.citizenshipFirstParent = citizenshipFirstParent;
    }

    public String getGenderFirstParent() {
        return genderFirstParent;
    }

    public void setGenderFirstParent(String genderFirstParent) {
        this.genderFirstParent = genderFirstParent;
    }

    public String getBirthDateFirstParent() {
        return birthDateFirstParent;
    }

    public void setBirthDateFirstParent(String birthDateFirstParent) {
        this.birthDateFirstParent = birthDateFirstParent;
    }

    public String getCitizenStatusFirstParent() {
        return citizenStatusFirstParent;
    }

    public void setCitizenStatusFirstParent(String citizenStatusFirstParent) {
        this.citizenStatusFirstParent = citizenStatusFirstParent;
    }

    public String getBirthNameSecondParent() {
        return birthNameSecondParent;
    }

    public void setBirthNameSecondParent(String birthNameSecondParent) {
        this.birthNameSecondParent = birthNameSecondParent;
    }

    public String getNameSecondParent() {
        return nameSecondParent;
    }

    public void setNameSecondParent(String nameSecondParent) {
        this.nameSecondParent = nameSecondParent;
    }

    public String getFirstSurnameSecondParent() {
        return firstSurnameSecondParent;
    }

    public void setFirstSurnameSecondParent(String firstSurnameSecondParent) {
        this.firstSurnameSecondParent = firstSurnameSecondParent;
    }

    public String getSecondSurnameSecondParent() {
        return secondSurnameSecondParent;
    }

    public void setSecondSurnameSecondParent(String secondSurnameSecondParent) {
        this.secondSurnameSecondParent = secondSurnameSecondParent;
    }

    public String getOccupationSecondParent() {
        return occupationSecondParent;
    }

    public void setOccupationSecondParent(String occupationSecondParent) {
        this.occupationSecondParent = occupationSecondParent;
    }

    public String getIdentificationCardSecondParent() {
        return identificationCardSecondParent;
    }

    public void setIdentificationCardSecondParent(String identificationCardSecondParent) {
        this.identificationCardSecondParent = identificationCardSecondParent;
    }

    public String getCredentialNumberSecondParent() {
        return credentialNumberSecondParent;
    }

    public void setCredentialNumberSecondParent(String credentialNumberSecondParent) {
        this.credentialNumberSecondParent = credentialNumberSecondParent;
    }

    public String getCitizenshipSecondParent() {
        return citizenshipSecondParent;
    }

    public void setCitizenshipSecondParent(String citizenshipSecondParent) {
        this.citizenshipSecondParent = citizenshipSecondParent;
    }

    public String getGenderSecondParent() {
        return genderSecondParent;
    }

    public void setGenderSecondParent(String genderSecondParent) {
        this.genderSecondParent = genderSecondParent;
    }

    public String getBirthDateSecondParent() {
        return birthDateSecondParent;
    }

    public void setBirthDateSecondParent(String birthDateSecondParent) {
        this.birthDateSecondParent = birthDateSecondParent;
    }

    public String getCitizenStatusSecondParent() {
        return citizenStatusSecondParent;
    }

    public void setCitizenStatusSecondParent(String citizenStatusSecondParent) {
        this.citizenStatusSecondParent = citizenStatusSecondParent;
    }

    public String getOfficerName() {
        return officerName;
    }

    public void setOfficerName(String officerName) {
        this.officerName = officerName;
    }

    public String getIssuancePlace() {
        return issuancePlace;
    }

    public void setIssuancePlace(String issuancePlace) {
        this.issuancePlace = issuancePlace;
    }

    public String getIssuingAuthority() {
        return issuingAuthority;
    }

    public void setIssuingAuthority(String issuingAuthority) {
        this.issuingAuthority = issuingAuthority;
    }

    public String getIssuanceCountry() {
        return issuanceCountry;
    }

    public void setIssuanceCountry(String issuanceCountry) {
        this.issuanceCountry = issuanceCountry;
    }

    private void readFromParcel(Parcel in) {
        fcn = in.readString();
        tcn = in.readString();
        icn = in.readString();
        name = in.readString();
        firstSurname = in.readString();
        secondSurname = in.readString();
        crn = in.readString();
        gender = in.readString();
        birthDate = in.readString();
        birthTime = in.readString();
        issuanceDate = in.readString();
        idDocument = in.readString();

        birthPlace = in.readString();
        city = in.readString();
        street = in.readString();
        number = in.readString();

        birthNameFirstParent = in.readString();
        nameFirstParent = in.readString();
        firstSurnameFirstParent = in.readString();
        secondSurnameFirstParent = in.readString();
        occupationFirstParent = in.readString();
        identificationCardFirstParent = in.readString();
        credentialNumberFirstParent = in.readString();
        citizenshipFirstParent = in.readString();
        genderFirstParent = in.readString();
        birthDateFirstParent = in.readString();
        citizenStatusFirstParent = in.readString();

        birthNameSecondParent = in.readString();
        nameSecondParent = in.readString();
        firstSurnameSecondParent = in.readString();
        secondSurnameSecondParent = in.readString();
        occupationSecondParent = in.readString();
        identificationCardSecondParent = in.readString();
        credentialNumberSecondParent = in.readString();
        citizenshipSecondParent = in.readString();
        genderSecondParent = in.readString();
        birthDateSecondParent = in.readString();
        citizenStatusSecondParent = in.readString();

        officerName = in.readString();
        issuancePlace = in.readString();
        issuingAuthority = in.readString();
        issuanceCountry = in.readString();

    }

    @Override
    public String toString() {
        return "BirthCertificateMessage [fcn=" + fcn + ", tcn=" + tcn + ", icn=" + icn + ", name=" + name
                + ", firstSurname=" + firstSurname + ", secondSurname=" + secondSurname + ", crn=" + crn
                + ", gender=" + gender + ", birthDate=" + birthDate + ", birthTime=" + birthTime + ", issuanceDate="
                + issuanceDate + ", idDocument=" + idDocument + ", birthPlace=" + birthPlace + ", city=" + city + ", street="
                + street + ", number=" + number + ", birthNameFirstParent=" + birthNameFirstParent
                + ", nameFirstParent=" + nameFirstParent + ", firstSurnameFirstParent=" + firstSurnameFirstParent
                + ", secondSurnameFirstParent=" + secondSurnameFirstParent + ", occupationFirstParent="
                + occupationFirstParent + ", identificationCardFirstParent=" + identificationCardFirstParent
                + ", credentialNumberFirstParent=" + credentialNumberFirstParent + ", citizenshipFirstParent="
                + citizenshipFirstParent + ", genderFirstParent=" + genderFirstParent + ", birthDateFirstParent="
                + birthDateFirstParent + ", citizenStatusFirstParent=" + citizenStatusFirstParent
                + ", birthNameSecondParent=" + birthNameSecondParent + ", nameSecondParent=" + nameSecondParent
                + ", firstSurnameSecondParent=" + firstSurnameSecondParent + ", secondSurnameSecondParent="
                + secondSurnameSecondParent + ", occupationSecondParent=" + occupationSecondParent
                + ", identificationCardSecondParent=" + identificationCardSecondParent
                + ", credentialNumberSecondParent=" + credentialNumberSecondParent + ", citizenshipSecondParent="
                + citizenshipSecondParent + ", genderSecondParent=" + genderSecondParent + ", birthDateSecondParent="
                + birthDateSecondParent + ", citizenStatusSecondParent=" + citizenStatusSecondParent + ", officerName="
                + officerName + ", issuancePlace=" + issuancePlace + ", issuingAuthority=" + issuingAuthority
                + ", issuanceCountry=" + issuanceCountry + "]";
    }

}
