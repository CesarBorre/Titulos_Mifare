package com.neo.titulos.utils;

import org.spongycastle.crypto.BlockCipher;
import org.spongycastle.crypto.engines.AESEngine;
import org.spongycastle.crypto.macs.CMac;
import org.spongycastle.crypto.params.KeyParameter;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by emercado on 11/11/15.
 */
public class AES256Cipher {
    static final String TAG = "eBreeder";

    public static byte[] encrypt(byte[] ivBytes, byte[] keyBytes, byte[] textBytes)
            throws java.io.UnsupportedEncodingException,
            NoSuchAlgorithmException,
            NoSuchPaddingException,
            InvalidKeyException,
            InvalidAlgorithmParameterException,
            IllegalBlockSizeException,
            BadPaddingException {

        //AlgorithmParameterSpec ivSpec = new IvParameterSpec(ivBytes);
        byte[] finalIvs = new byte[16];
        int len = ivBytes.length > 16 ? 16 : ivBytes.length;
        System.arraycopy(ivBytes, 0, finalIvs, 0, len);
        IvParameterSpec ivps = new IvParameterSpec(finalIvs);

        SecretKeySpec newKey = new SecretKeySpec(keyBytes, "AES");
        Cipher cipher = null;
        cipher = Cipher.getInstance("AES/CBC/NoPadding");
        cipher.init(Cipher.ENCRYPT_MODE, newKey, ivps);
        return cipher.doFinal(textBytes);
    }

    public static byte[] decrypt(byte[] ivBytes, byte[] keyBytes, byte[] textBytes)
            throws java.io.UnsupportedEncodingException,
            NoSuchAlgorithmException,
            NoSuchPaddingException,
            InvalidKeyException,
            InvalidAlgorithmParameterException,
            IllegalBlockSizeException,
            BadPaddingException {

        //IvParameterSpec ivSpec = new IvParameterSpec(ivBytes);
        byte[] finalIvs = new byte[16];
        int len = ivBytes.length > 16 ? 16 : ivBytes.length;
        System.arraycopy(ivBytes, 0, finalIvs, 0, len);
        AlgorithmParameterSpec ivps = new IvParameterSpec(finalIvs);

        SecretKeySpec newKey = new SecretKeySpec(keyBytes, "AES");
        Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");

        //Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, newKey, ivps);
        return cipher.doFinal(textBytes);
    }
    public static byte[] getCMAC(byte[] keybytes,byte[] msg,int lengthBits){
        BlockCipher cipher=new AESEngine();
        CMac cmac=new CMac(cipher);
        cmac.init(new KeyParameter(keybytes));
        cmac.update(msg, 0, msg.length);
        byte[] out=new byte[cmac.getMacSize()];
        cmac.doFinal(out, 0);
        return Arrays.copyOf(out, lengthBits / 8);
    }
}
