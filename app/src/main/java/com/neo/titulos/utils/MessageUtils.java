package com.neo.titulos.utils;

/**
 * Created by IlseParra on 20/11/2015.
 */
public class MessageUtils {

    public static String getField(int start, int end, String text){
        return getSubstring(start, end, text).trim();
    }
    private static String getSubstring(int start, int end, String text){
        return text.substring(start, end);
    }
}
