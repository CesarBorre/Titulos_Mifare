package com.neo.titulos.utils;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.TextView;

import com.neo.titulos.R;

/**
 * Created by root on 9/05/16.
 */
public class SnackBar {

    /**
     * Proyecta una {@link Snackbar} con el string usado
     *
     * @param msg Mensaje
     */
    public static void showSnackBar(String msg, final Activity a, int configType) {

        Snackbar snackbar = null;

        switch (configType) {
            case 0:
                snackbar = Snackbar
                        .make(a.findViewById(R.id.layoutID), msg, Snackbar.LENGTH_INDEFINITE)
                        .setAction(a.getResources().getString(R.string.activate_internet), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent = new Intent();
                                intent.setAction(Settings.ACTION_NFC_SETTINGS);
                                a.startActivity(intent);
                            }
                        });
                break;
            case 1:
                snackbar = Snackbar
                        .make(a.findViewById(R.id.coordinator), msg, Snackbar.LENGTH_LONG)
                        .setAction(a.getResources().getString(R.string.activate_internet), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent = new Intent();
                                intent.setAction(Settings.ACTION_WIFI_SETTINGS);
                                a.startActivity(intent);
                            }
                        });
                break;
        }


        // Changing message text color
        snackbar.setActionTextColor(Color.GREEN);

        // Changing action button text color
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.CYAN);
        snackbar.show();
    }
}
