package com.neo.titulos.utils;

import android.nfc.tech.IsoDep;
import android.util.Log;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/**
 * Created by emercado on 11/11/15.
 */
public class Sl3Util {
    private static final String RndA =  "00000000000000000000000000000000";
    private static final String VI =    "00000000000000000000000000000000";
    private static final String sPass = "6EEB6F6C40363E2565387233E95E2972";
    /**
     * Utility class to convert a byte array to a hexadecimal string.
     *
     * @param bytes Bytes to convert
     * @return String, containing hexadecimal representation.
     */
    public static String ByteArrayToHexString(byte[] bytes) {
        final char[] hexArray = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
        char[] hexChars = new char[bytes.length * 2];
        int v;
        for ( int j = 0; j < bytes.length; j++ ) {
            v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    /**
     * Utility class to convert a hexadecimal string to a byte string.
     *
     * <p>Behavior with input strings containing non-hexadecimal characters is undefined.
     *
     * @param s String containing hexadecimal characters to convert
     * @return Byte array generated from input
     */
    public static byte[] HexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }


    private static byte[] shiftRight(byte[] input){
        byte[] initial=new byte[input.length];
        byte lastByte=input[0];
        //Log.i(TAG,"Antes de shift: "+ByteArrayToHexString(initial));
        System.arraycopy(input, 1, initial, 0, input.length-1);
        initial[input.length-1]=lastByte;
        Log.i(Constants.TAG, "Despues de shift: " + ByteArrayToHexString(initial));
        return initial;
    }


    private static byte[] concatenateByteArrays(byte[] a, byte[] b){
        byte[] c = new byte[a.length + b.length];
        System.arraycopy(a, 0, c, 0, a.length);
        System.arraycopy(b, 0, c, a.length, b.length);
        Log.i(Constants.TAG, ByteArrayToHexString(c));
        return c;
    }

    private static String formatUid(String sUID){
        if (sUID.length()==14){
            sUID=sUID+sUID.substring(0,2);
        } else {
            sUID=sUID+sUID;
        }
        sUID =sUID+"0000000000000000";
        return sUID;
    }
    private static byte[] generateTagKey(String uid) throws NoSuchPaddingException, InvalidAlgorithmParameterException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, InvalidKeyException {
        Log.i(Constants.TAG, "UID: " + uid);
        uid=formatUid(uid);
        Log.i(Constants.TAG, "UID formated: " +uid );

        return AES256Cipher.encrypt(HexStringToByteArray(VI),  HexStringToByteArray(uid),HexStringToByteArray(sPass) );
    }

    private static byte[] getRndB(IsoDep isoDep) throws IOException {
        String strCommand="70004000";
        byte[] command = HexStringToByteArray(strCommand);

        // Send command to remote device
        Log.i(Constants.TAG, "Sending: " + ByteArrayToHexString(command));
        byte[] RndB = isoDep.transceive(command);
        // If AID is successfully selected, 0x9000 is returned as the status word (last 2
        // bytes of the result) by convention. Everything before the status word is
        // optional payload, which is used here to hold the account number.
        //Log.i(TAG, "RndB: " + ByteArrayToHexString(RndB));

        byte [] RndB_2 = HexStringToByteArray(ByteArrayToHexString(RndB).substring(2));
        Log.i(Constants.TAG, "RndB: " + ByteArrayToHexString(RndB_2));
        return RndB_2;
    }

    private static byte[] getStatusCodeEncrypted(IsoDep isoDep, byte[] tagKey, byte[] RndB2Shifted) throws NoSuchPaddingException, InvalidAlgorithmParameterException, IOException, IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, InvalidKeyException {
        byte[] firstAuth = AES256Cipher.encrypt(HexStringToByteArray(VI),tagKey,concatenateByteArrays(HexStringToByteArray(RndA),RndB2Shifted));
        Log.i(Constants.TAG, "Encrypted: " + ByteArrayToHexString(firstAuth));
        String command2 = "72"+ByteArrayToHexString(firstAuth);
        Log.i(Constants.TAG, "Sending: " + command2);
        byte [] result = isoDep.transceive(HexStringToByteArray(command2));
        result = HexStringToByteArray(ByteArrayToHexString(result).substring(2));
        return result;
    }
    public static byte[] xor(byte[] a, byte[] b) {
        byte[] result = new byte[Math.min(a.length, b.length)];

        for (int i = 0; i < result.length; i++) {
            result[i] = (byte) (((int) a[i]) ^ ((int) b[i]));
        }

        return result;
    }
    public static byte[] readDataFromTag(IsoDep isoDep, int start, int lenght,int intNumberOfRead, HashMap<String, Object> sessionData){


        byte[] numberOfRead = new byte[]{(byte) (intNumberOfRead-1), 0x00};
        byte[] readSize = new byte[] { (byte)start, 0x00, (byte)lenght  };
        Log.e(Constants.TAG, "Blocks to read:"+ByteArrayToHexString(readSize));
        Log.e(Constants.TAG, "Read number:"+ByteArrayToHexString(numberOfRead));
        //String readingSize="00001E";

            try {


                //MAC_FUL
                // 33 CONTADOR(2 bytes) 0000  - 0100 -> 0200 HEXADECIMAL
                // 280010   - BLOQUE en hexa -00- NO BLOQUES
                byte[] mac_full=AES256Cipher.getCMAC((byte[])sessionData.get("kmac"), concatenateByteArrays(HexStringToByteArray("33"+ByteArrayToHexString(numberOfRead)), concatenateByteArrays((byte[])sessionData.get("TI"), readSize)), 128);
                //Log.i(Constants.TAG, "mac_full: " + ByteArrayToHexString(mac_full));
                byte[] mac_even={mac_full[1],mac_full[3],mac_full[5],mac_full[7],mac_full[9],mac_full[11],mac_full[13],mac_full[15]};
                // Log.i(Constants.TAG, "mac_even: " + ByteArrayToHexString(mac_even));

                // 33 BLOQUE en hexa -00- NO BLOQUES
                byte[] commandRead=concatenateByteArrays(HexStringToByteArray("33" + ByteArrayToHexString(readSize)), mac_even);
                //Log.i(Constants.TAG, "commandRead: " + ByteArrayToHexString(commandRead));
                byte [] result=isoDep.transceive(commandRead);
                //Log.i(Constants.TAG, "result: " + ByteArrayToHexString(result));
                String x=ByteArrayToHexString(result);
                // Log.i(Constants.TAG, "result2: " + x.substring(2, x.length() - 16));
                String dataReaded= x.substring(2, x.length() - 16);
                // Log.i(Constants.TAG, "dataReaded lenght: " + dataReaded.length());

                //Log.i(Constants.TAG, "DATOS:" + new String(HexStringToByteArray(dataReaded)));
                return HexStringToByteArray(dataReaded);

            } catch (IOException e) {
                Log.e(Constants.TAG, "Error communicating with card: " + e.toString());
            }catch (Exception e) {
                e.printStackTrace();
                Log.e(Constants.TAG, "Error general: " + e.toString());
            }


        return null;
    }
    public static HashMap<String, Object>  authenticate(IsoDep isoDep){

        //String readingSize="00001E";
        HashMap<String, Object> sessionData = new HashMap<String, Object>();
        try {

            // Connect to the remote NFC device
            // get list of technologies
            String[] technologies = isoDep.getTag().getTechList();
            // get tagKey
            String uid = ByteArrayToHexString(isoDep.getTag().getId());
            byte[] tagKey = generateTagKey(uid);
            Log.i(Constants.TAG, "tagKey: " + ByteArrayToHexString(tagKey));
            // getRndB
            byte [] RndB_2 = getRndB(isoDep);
            // decrypt RndB
            byte[] RndBDecrypted = AES256Cipher.decrypt(HexStringToByteArray(VI), tagKey, RndB_2);
            Log.i(Constants.TAG, "RndB 2: " + ByteArrayToHexString(RndBDecrypted));
            byte[] RndB2Shifted = shiftRight(RndBDecrypted);

            // get status code encrypted
            byte[] statusCode= getStatusCodeEncrypted(isoDep, tagKey, RndB2Shifted);
            Log.i(Constants.TAG, "statusCode encrypted: " + ByteArrayToHexString(statusCode));
            // decrypt statusCode
            byte[] statusCodeDecrypted= AES256Cipher.decrypt(HexStringToByteArray(VI), tagKey, statusCode);
            //Log.i(Constants.TAG, "statusCode decrypted: " + ByteArrayToHexString(statusCodeDecrypted));
            // get Transaction Identifier
            byte[] TI= Arrays.copyOfRange(statusCodeDecrypted, 0, 4);
            Log.i(Constants.TAG, "TI: " + ByteArrayToHexString(TI));
            sessionData.put("TI",TI);

            byte [] xorKenc=xor(Arrays.copyOfRange(HexStringToByteArray(RndA), 4, 9),
                    Arrays.copyOfRange(RndBDecrypted, 4, 9));
            //Log.i(Constants.TAG,"ByteArrayToHexString(Arrays.copyOfRange(RndBDecrypted, 4, 9)) of "+ ByteArrayToHexString(RndBDecrypted));
            //Log.i(Constants.TAG, "xor: " +ByteArrayToHexString(Arrays.copyOfRange(HexStringToByteArray(RndA), 4, 9))+" xor "+ByteArrayToHexString(Arrays.copyOfRange(RndBDecrypted, 4, 9)));

            //Log.i(Constants.TAG, "xorKenc: " + ByteArrayToHexString(xorKenc));

            byte[] base_kenc=concatenateByteArrays(concatenateByteArrays(Arrays.copyOfRange(HexStringToByteArray(RndA), 11, 16),
                            Arrays.copyOfRange(RndBDecrypted, 11, 16)),
                    concatenateByteArrays(xorKenc, HexStringToByteArray("11")));
            //Log.i(Constants.TAG, "base_kenc: " + ByteArrayToHexString(base_kenc));

            byte[] kenc=AES256Cipher.encrypt(HexStringToByteArray(VI), tagKey, base_kenc);
            //Log.i(Constants.TAG, "kenc: " + ByteArrayToHexString(kenc));

            byte [] xorKmac=xor(Arrays.copyOfRange(HexStringToByteArray(RndA), 0, 5),
                    Arrays.copyOfRange(RndBDecrypted, 0, 5));
            //Log.i(Constants.TAG, "xorKmac: " + ByteArrayToHexString(xorKmac));

            byte[] base_kmac=concatenateByteArrays(concatenateByteArrays(Arrays.copyOfRange(HexStringToByteArray(RndA), 7, 12),
                            Arrays.copyOfRange(RndBDecrypted, 7, 12)),
                    concatenateByteArrays(xorKmac, HexStringToByteArray("22")));
            //Log.i(Constants.TAG, "base_kmac: " + ByteArrayToHexString(base_kmac));
            byte[] kmac=AES256Cipher.encrypt(HexStringToByteArray(VI), tagKey, base_kmac);
            //Log.i(Constants.TAG, "kmac: " + ByteArrayToHexString(kmac));
            sessionData.put("kmac", kmac);
            return sessionData;

        } catch (IOException e) {
            Log.e(Constants.TAG, "Error communicating with card: " + e.toString());
        }catch (Exception e) {
            e.printStackTrace();
            Log.e(Constants.TAG, "Error general: " + e.toString());
        }


        return null;
    }


    /*public static byte[] readDataFromTag(IsoDep isoDep, int start, int lenght, byte[] numberOfRead){

        byte[] readSize = new byte[] { (byte)start, 0x00, (byte)lenght  };
        Log.e(Constants.TAG, "Blocks to read:"+ByteArrayToHexString(readSize));
        Log.e(Constants.TAG, "Read number:"+ByteArrayToHexString(numberOfRead));
        //String readingSize="00001E";

        try {

            // Connect to the remote NFC device
            // get list of technologies
            String[] technologies = isoDep.getTag().getTechList();
            // get tagKey
            String uid = ByteArrayToHexString(isoDep.getTag().getId());
            byte[] tagKey = generateTagKey(uid);
            Log.i(Constants.TAG, "tagKey: " + ByteArrayToHexString(tagKey));
            // getRndB
            byte [] RndB_2 = getRndB(isoDep);
            // decrypt RndB
            byte[] RndBDecrypted = AES256Cipher.decrypt(HexStringToByteArray(VI), tagKey, RndB_2);
            Log.i(Constants.TAG, "RndB 2: " + ByteArrayToHexString(RndBDecrypted));
            byte[] RndB2Shifted = shiftRight(RndBDecrypted);

            // get status code encrypted
            byte[] statusCode= getStatusCodeEncrypted(isoDep, tagKey, RndB2Shifted);
            Log.i(Constants.TAG, "statusCode encrypted: " + ByteArrayToHexString(statusCode));
            // decrypt statusCode
            byte[] statusCodeDecrypted= AES256Cipher.decrypt(HexStringToByteArray(VI), tagKey, statusCode);
            Log.i(Constants.TAG, "statusCode decrypted: " + ByteArrayToHexString(statusCodeDecrypted));
            // get Transaction Identifier
            byte[] TI= Arrays.copyOfRange(statusCodeDecrypted, 0, 4);
            Log.i(Constants.TAG, "TI: " + ByteArrayToHexString(TI));

            byte [] xorKenc=xor(Arrays.copyOfRange(HexStringToByteArray(RndA), 4, 9),
                    Arrays.copyOfRange(RndBDecrypted, 4, 9));
            Log.i(Constants.TAG,"ByteArrayToHexString(Arrays.copyOfRange(RndBDecrypted, 4, 9)) of "+ ByteArrayToHexString(RndBDecrypted));
            Log.i(Constants.TAG, "xor: " +ByteArrayToHexString(Arrays.copyOfRange(HexStringToByteArray(RndA), 4, 9))+" xor "+ByteArrayToHexString(Arrays.copyOfRange(RndBDecrypted, 4, 9)));

            Log.i(Constants.TAG, "xorKenc: " + ByteArrayToHexString(xorKenc));

            byte[] base_kenc=concatenateByteArrays(concatenateByteArrays(Arrays.copyOfRange(HexStringToByteArray(RndA), 11, 16),
                            Arrays.copyOfRange(RndBDecrypted, 11, 16)),
                    concatenateByteArrays(xorKenc, HexStringToByteArray("11")));
            Log.i(Constants.TAG, "base_kenc: " + ByteArrayToHexString(base_kenc));

            byte[] kenc=AES256Cipher.encrypt(HexStringToByteArray(VI), tagKey, base_kenc);
            Log.i(Constants.TAG, "kenc: " + ByteArrayToHexString(kenc));

            byte [] xorKmac=xor(Arrays.copyOfRange(HexStringToByteArray(RndA), 0, 5),
                    Arrays.copyOfRange(RndBDecrypted, 0, 5));
            Log.i(Constants.TAG, "xorKmac: " + ByteArrayToHexString(xorKmac));

            byte[] base_kmac=concatenateByteArrays(concatenateByteArrays(Arrays.copyOfRange(HexStringToByteArray(RndA), 7, 12),
                            Arrays.copyOfRange(RndBDecrypted, 7, 12)),
                    concatenateByteArrays(xorKmac, HexStringToByteArray("22")));
            Log.i(Constants.TAG, "base_kmac: " + ByteArrayToHexString(base_kmac));
            byte[] kmac=AES256Cipher.encrypt(HexStringToByteArray(VI), tagKey, base_kmac);
            Log.i(Constants.TAG, "kmac: " + ByteArrayToHexString(kmac));
            //MAC_FUL
            // 33 CONTADOR(2 bytes) 0000  - 0100 -> 0200 HEXADECIMAL
            // 280010   - BLOQUE en hexa -00- NO BLOQUES
            byte[] mac_full=AES256Cipher.getCMAC(kmac, concatenateByteArrays(HexStringToByteArray("33"+ByteArrayToHexString(numberOfRead)), concatenateByteArrays(TI, readSize)), 128);
            Log.i(Constants.TAG, "mac_full: " + ByteArrayToHexString(mac_full));
            byte[] mac_even={mac_full[1],mac_full[3],mac_full[5],mac_full[7],mac_full[9],mac_full[11],mac_full[13],mac_full[15]};
            Log.i(Constants.TAG, "mac_even: " + ByteArrayToHexString(mac_even));

            // 33 BLOQUE en hexa -00- NO BLOQUES
            byte[] commandRead=concatenateByteArrays(HexStringToByteArray("33" + ByteArrayToHexString(readSize)), mac_even);
            Log.i(Constants.TAG, "commandRead: " + ByteArrayToHexString(commandRead));
            byte [] result=isoDep.transceive(commandRead);
            Log.i(Constants.TAG, "result: " + ByteArrayToHexString(result));
            String x=ByteArrayToHexString(result);
            Log.i(Constants.TAG, "result2: " + x.substring(2, x.length() - 16));
            String dataReaded= x.substring(2, x.length() - 16);
            Log.i(Constants.TAG, "dataReaded lenght: " + dataReaded.length());

            Log.i(Constants.TAG, "DATOS:" + new String(HexStringToByteArray(dataReaded)));
            return HexStringToByteArray(dataReaded);

        } catch (IOException e) {
            Log.e(Constants.TAG, "Error communicating with card: " + e.toString());
        }catch (Exception e) {
            e.printStackTrace();
            Log.e(Constants.TAG, "Error general: " + e.toString());
        }


        return null;
    }*/
}
