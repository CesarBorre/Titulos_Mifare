package com.neo.titulos;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.neo.titulos.dialogs.ConfirmRecord_Dialog;
import com.neo.titulos.dialogs.Settings_Dialog;
import com.neo.titulos.utils.Constants;
import com.nxp.nfclib.classic.IMFClassic;
import com.nxp.nfclib.classic.IMFClassicEV1;
import com.nxp.nfclib.exceptions.CloneDetectedException;
import com.nxp.nfclib.exceptions.ReaderException;
import com.nxp.nfclib.utils.Utilities;

import com.nxp.nfcliblite.NxpNfcLibLite;
import com.nxp.nfcliblite.Nxpnfcliblitecallback;
import com.nxp.nfcliblite.cards.IPlus;

public class ReadMifare_v2 extends AppCompatActivity {

    public static String TAG = ReadMifare_v2.class.getSimpleName();
    private NxpNfcLibLite libInstance = null;

    boolean resLectura = false;

    int iConta = 0;
    /**
     * Mifare MFClassic instance initiated.
     */
    private IMFClassic classic;
    /**
     * Mifare Plus instance initiated.
     */
    private IPlus plus;

    private enum EnumCardType {
        EnumPlus,
        EnumNTag213215216,
        EnumClassicEV1,
        EnumNone
    }

    String UID = null;
    SharedPreferences sharedPreferences;
    ProgressDialog dialog;

    private EnumCardType mCardType = EnumCardType.EnumNone;

    private boolean mIsPerformingCardOperations = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read_tag);
        sharedPreferences = getApplicationContext().getSharedPreferences(Constants.SHARED_NAME, MODE_PRIVATE);
        setToolbar();
        libInstance = NxpNfcLibLite.getInstance();
        libInstance.registerActivity(this);
        initProgressDialog();
    }

    private void setToolbar() {
        // Añadir la Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarRead);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
    }

    private void initProgressDialog() {
        dialog = new ProgressDialog(ReadMifare_v2.this);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setTitle("Leyengo TAG");
        dialog.setMessage("Espere, por favor...");
        dialog.setCanceledOnTouchOutside(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
//        MenuItem item = menu.findItem(R.id.action_login);
//        item.setActionView(R.layout.item_menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_settings:
                showDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void showDialog() {
        DialogFragment dialogFragment = Settings_Dialog.newInstance();
        dialogFragment.show(getSupportFragmentManager(), "dialog");
    }

    @Override
    protected void onPause() {
        libInstance.stopForeGroundDispatch();
        super.onPause();
    }

    @Override
    protected void onResume() {
        libInstance.startForeGroundDispatch();
        super.onResume();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        resLectura = false;
        iConta = 0;
        dialog.show();
        if (intent != null) {
            try {
                libInstance.filterIntent(intent, new Nxpnfcliblitecallback() {

                    @Override
                    public void onClassicEV1CardDetected(IMFClassicEV1 imfClassicEV1) {

                    }

                    @Override
                    public void onPlusCardDetected(final IPlus iPlus) {
                        if (mCardType == EnumCardType.EnumPlus && mIsPerformingCardOperations) {
                            //Already Some Operations are happening in the same card, discard the callback
                            Log.d(TAG, "----- Already Some Operations are happening in the same card, discard the callback: " + mCardType.toString());
                            return;
                        }
                        mIsPerformingCardOperations = true;
                        mCardType = EnumCardType.EnumPlus;

                        plus = iPlus;
                        try {
                            plus.getReader().connect();
    //                        plusCardLogic();
                            iConta++;
                            if (Utilities.dumpBytes(plus.getCardDetails().uid) != null) {
                                Log.i(TAG, "Card Detected: " + plus.getCardDetails().cardName + " "
                                        + plus.getCardDetails().securityLevel + Utilities.dumpBytes(plus.getCardDetails().uid));
                                if (iConta == 2) {
                                    getSharedUid(Utilities.dumpBytes(plus.getCardDetails().uid));
                                    resLectura = true;
                                }

                            } else {
                                Toast.makeText(getApplicationContext(), "Intente de nuevo la lectura del TAG", Toast.LENGTH_SHORT).show();
                            }

                            plus.getReader().close();
                        } catch (ReaderException e1) {
                            e1.printStackTrace();
                            Log.e(TAG, "Unknown Error Tap Again!");
                            Toast.makeText(getApplicationContext(), "Intente de nuevo la lectura del TAG", Toast.LENGTH_SHORT).show();
                        } catch (Throwable t) {
                            t.printStackTrace();
                            Log.e(TAG, "Unknown Error Tap Again!");
                            Toast.makeText(getApplicationContext(), "Intente de nuevo la lectura del TAG", Toast.LENGTH_SHORT).show();
                        }
                        mIsPerformingCardOperations = false;

                    }
                });
            } catch (CloneDetectedException e) {
                e.printStackTrace();
            }

        } else {
            Toast.makeText(getApplicationContext(), "Intentar de nuevo", Toast.LENGTH_SHORT).show();
        }

        if (!resLectura) {
            Toast.makeText(getApplicationContext(), "Intentar lectura de nuevo", Toast.LENGTH_SHORT).show();
        }
    }

    private void getSharedUid(String UID) {
        dialog.dismiss();
        if (!sharedPreferences.contains(UID)) {
            //buscar en json definido
            //read_UIDjson(Read_UID_Doc.uid_item, UID);
            showPersoDialog();
        } else {
            //mostrar contenido
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            intent.putExtra("UID", sharedPreferences.getString(UID, null));
            startActivity(intent);
        }
    }

    public void showPersoDialog() {
        DialogFragment dialogFragment = ConfirmRecord_Dialog.newInstance();
        dialogFragment.show(getSupportFragmentManager(), "dialog");
    }
}
