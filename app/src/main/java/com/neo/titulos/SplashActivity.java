package com.neo.titulos;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.io.InputStream;

import com.neo.titulos.mifareplus.jsonparsing.DocumentStructure;

public class SplashActivity extends AppCompatActivity {
    public static final String TAG = SplashActivity.class.getSimpleName();
    private CountDownTimer mCountDownTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        if (!isTaskRoot()) {
            final Intent intent = getIntent();
            if (intent.hasCategory(Intent.CATEGORY_LAUNCHER) && Intent.ACTION_MAIN.equals(intent.getAction())) {
                Log.w(TAG, "Splash Activity is not the root.  Finishing Splash Activity instead of launching.");
                finish();
                return;
            }
        }

        InputStream documentType = getResources().openRawResource(R.raw.document_type_map);
        InputStream birthCert = getResources().openRawResource(R.raw.birth_certificate_map);

        DocumentStructure.createDocumentStructures(documentType, birthCert);
        DocumentStructure.cleanData();

        mCountDownTimer = new CountDownTimer(1500, 500) {
            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                Intent intent = new Intent(SplashActivity.this, ReadMifare_v2.class);
                startActivity(intent);
                overridePendingTransition(R.animator.animation, 0);
                finish();
            }
        }.start();
    }

    @Override
    public void onBackPressed() {
        mCountDownTimer.cancel();
        super.onBackPressed();
    }

    @Override
    protected void onStop() {
        mCountDownTimer.cancel();
        super.onStop();
    }
}
