package com.neo.titulos;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.neo.titulos.mifareplus.jsonparsing.DocumentStructure;
import com.neo.titulos.model.BirthCertificateMessage;
import com.neo.titulos.model.Certificados;
import com.neo.titulos.model.Titulos_WS_Data;
import com.neo.titulos.utils.CheckInternetConnection;
import com.neo.titulos.utils.SnackBar;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {
    static final String TAG = MainActivity.class.getSimpleName();

    ProgressDialog progressDialog;

    TextView titulo;
    TextView nombre;
    TextView carrera;
    TextView nivel;
    TextView numIdentificacion;
    TextView clave;
    ImageView foto;

    BirthCertificateMessage birthCertificateMessage;

    private String icn;
    String UID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setToolbar();
        Intent intent = getIntent();
        UID = intent.getStringExtra("UID");
        //birthCertificateMessage = intent.getParcelableExtra("birthCertificate");
        initElements();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
//        MenuItem item = menu.findItem(R.id.action_login);
//        item.setActionView(R.layout.item_menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.buscar_item:
                searchData();
                return true;
//            case R.id.action_settings:
//                showDialog_Settings();
////                SnackBar.showSnackBar("Añadir a contactos", this, 1);
//                return true;
//            case R.id.action_favorite:
//                showSnackBar("Añadir a favoritos");
//                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initElements() {
        initProgress();
        titulo = (TextView) findViewById(R.id.tituloID);
        nombre = (TextView) findViewById(R.id.nombreID);
        carrera = (TextView) findViewById(R.id.carreraID);
        nivel = (TextView) findViewById(R.id.nivelID);
        numIdentificacion = (TextView) findViewById(R.id.numIdentificacionID);
        clave = (TextView) findViewById(R.id.claveEscuela);
        foto = (ImageView) findViewById(R.id.fotoMiniaturaID);
        String datosTitulo [] = UID.split("\\|");

        if (datosTitulo[1].equals("0")) {
            icn = "0048420150001327";
            setInfoEvi();
        } else {
            icn = "0484201500010326";
        }


        /*
        if (birthCertificateMessage != null) {
            Log.d(TAG, "ICN "+birthCertificateMessage.getIcn());
            if (birthCertificateMessage.getIcn().equals("0048420150001327") ||
                    birthCertificateMessage.getIcn().equals("0484201500010329") ||
                    birthCertificateMessage.getIcn().equals("0484201500010330") ||
                    birthCertificateMessage.getIcn().equals("0484201500010331") ||
                    birthCertificateMessage.getIcn().equals("0484201500010332") ||
                    birthCertificateMessage.getIcn().equals("0484201500010333") ||
                    birthCertificateMessage.getIcn().equals("0484201500010334") ||
                    birthCertificateMessage.getIcn().equals("0484201500010335") ||
                    birthCertificateMessage.getIcn().equals("0484201500010328")) {
                icn = "0048420150001327";
                setInfoEvi();
            } else {
                icn = "0484201500010326";
            }
        } else if (DocumentStructure.tagData.get("icn") != null) {
            Log.d(TAG, "ICN "+ DocumentStructure.tagData.get("icn"));
            if (DocumentStructure.tagData.get("icn").equals("484201500010327")||
                    DocumentStructure.tagData.get("icn").equals("0484201500010329")||
                    DocumentStructure.tagData.get("icn").equals("0484201500010330")||
                    DocumentStructure.tagData.get("icn").equals("0484201500010331")||
                    DocumentStructure.tagData.get("icn").equals("0484201500010332")||
                    DocumentStructure.tagData.get("icn").equals("0484201500010333")||
                    DocumentStructure.tagData.get("icn").equals("0484201500010334")||
                    DocumentStructure.tagData.get("icn").equals("0484201500010335")||
                    DocumentStructure.tagData.get("icn").equals("0484201500010328")) {
                icn = "484201500010327";
                setInfoEvi();
            } else {
                icn = "0484201500010326";
            }
        }*/
    }

    private void setInfoEvi() {
        titulo.setText("LICENCIADA EN BIBLIOTECOLOGÍA");
        nombre.setText("VELASQUEZ RODRIGUEZ EVI DORIS");
        carrera.setText("BIBLIOTECOLOGÍA");
        nivel.setText("LICENCIATURA");
        numIdentificacion.setText("5970578");
        foto.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.mujer_bn));
    }

    private void initProgress() {
        progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setTitle(getResources().getString(R.string.title_progress_consulta));
        progressDialog.setMessage(getResources().getString(R.string.msg_progress));
    }

    private void searchData() {
        if (CheckInternetConnection.isConnectedToInternet(getApplicationContext())) {
            getJson();
        } else {
            SnackBar.showSnackBar(getResources().getString(R.string.no_internet), this, 1);
        }
    }

    private void getJson() {
        progressDialog.show();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(
                Request.Method.GET,
//                "http://52.39.221.86:8080/api/titulo?intNumeroIdentificacion=2",
                "http://mobile.neology-demos.com:8080/api/titulo?icn="+icn,
//                "http://" + preferences.Get_stringfrom_shprf(Constants_Settings.KEY_URL) + "/api/carneAll?intNumeroIdentificacion=4842015000102016",
                null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                        readJson(response.toString());
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                VolleyLog.e(TAG, "Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), "Error al conectar con servidor", Toast.LENGTH_SHORT).show();
            }
        });

        // Adding request to request queue
        VolleyApp.getmInstance().addToRequestQueue(jsonObjReq);
    }

    private void readJson(String json) {
        WS ws = new WS();
        ws.execute(json);
    }

    private void setToolbar() {
        // Añadir la Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

//        if (getSupportActionBar() != null) // Habilitar up button
//            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

//        CollapsingToolbarLayout collapser =
//                (CollapsingToolbarLayout) findViewById(R.id.collapser);
//        collapser.setTitle("UMSA"); // Cambiar título
    }

    class WS extends AsyncTask<String, Void, Boolean> {
        Titulos_WS_Data titulosData;
        Certificados certificados;

        @Override
        protected Boolean doInBackground(String... params) {
            try {
                JSONObject object = new JSONObject(params[0]);
                JSONObject jsonObject = object.getJSONObject("titulos");
                titulosData = new Titulos_WS_Data(
                        jsonObject.getInt("intNumeroIdentificacion"),
                        jsonObject.getString("strNombre"),
                        jsonObject.getString("strApePaterno"),
                        jsonObject.getString("strApeMaterno"),
                        jsonObject.getString("strTitulo"),
                        jsonObject.getString("strCarrera"),
                        jsonObject.getString("strNivel"),
                        jsonObject.getInt("intNumFolioDocumento"),
                        jsonObject.getString("strNacionalidad"),
                        jsonObject.getString("genero"),
                        jsonObject.getLong("dFechaNac"),
                        jsonObject.getLong("dFechaExpedicion"),
                        jsonObject.getString("strNombreUniversidad"),
                        jsonObject.getInt("intClaveUniversidad"),
                        jsonObject.getString("bFotoOriginal").getBytes(),
                        jsonObject.getString("bFotoMiniatura").getBytes(),
                        jsonObject.getString("bFirmaTitulado").getBytes()
                );

                JSONObject jsonCert = jsonObject.getJSONObject("certificados");
                certificados = new Certificados(
                        jsonCert.getInt("intNumeroIdentificacionCert"),
                        jsonCert.getInt("intPlanEstudios"),
                        jsonCert.getInt("intCreditosObligatorios"),
                        jsonCert.getInt("intCreditosOptativos"),
                        jsonCert.getInt("intCreditosTotales"),
                        jsonCert.getInt("intAsignaturasAprobadas"),
                        jsonCert.getInt("intAsignaturasNoAprobadas"),
                        jsonCert.getInt("intAsignaturasTotal"),
                        jsonCert.getDouble("dPromedio"),
                        jsonCert.getString("carrera"),
                        jsonCert.getString("plantel"),
                        jsonCert.getLong("fechaIngreso")
                );

            } catch (JSONException e) {
                e.printStackTrace();
                return false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean b) {
            super.onPostExecute(b);
            progressDialog.dismiss();
            if (b) {
                Intent intent = new Intent(getApplicationContext(), DataActivity.class);
                intent.putExtra("titulosData", titulosData);
                intent.putExtra("certificados", certificados);
                startActivity(intent);
            }
        }
    }
}