package com.neo.titulos.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.neo.titulos.model.Certificados;
import com.neo.titulos.model.Titulos_WS_Data;
import com.neo.titulos.subfragments.CertificadoFrag;
import com.neo.titulos.subfragments.TituloFrag;


/**
 * Created by Cesar on 02/06/2016.
 */
public class PagerAdapter extends FragmentPagerAdapter {
    Titulos_WS_Data titulos_ws_data;
    Certificados certificados;
    Context c;

    String tabs[] = {"TITULO", "CERTIFICADO"};

    public PagerAdapter(FragmentManager fm, Context c, Titulos_WS_Data titulos_ws_data, Certificados certificados) {
        super(fm);
        this.c = c;
        this.titulos_ws_data = titulos_ws_data;
        this.certificados = certificados;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                Bundle args = new Bundle();
                args.putSerializable("titulos_ws_data", titulos_ws_data);
                fragment = new TituloFrag();
                fragment.setArguments(args);
                break;
            case 1:
                Bundle args1 = new Bundle();
                args1.putSerializable("titulos_ws_data", titulos_ws_data);
                args1.putParcelable("certificados", certificados);
                fragment = new CertificadoFrag();
                fragment.setArguments(args1);
                break;
        }
        return fragment;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabs[position];
//        Drawable drawable = ContextCompat.getDrawable(c, R.mipmap.ic_launcher);
//        drawable.setBounds(0, 0, 45, 45);
//        ImageSpan imageSpan = new ImageSpan(drawable);
//        SpannableString spannableString = new SpannableString("  ");
//        spannableString.setSpan(imageSpan, 0, spannableString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
//        return spannableString;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
