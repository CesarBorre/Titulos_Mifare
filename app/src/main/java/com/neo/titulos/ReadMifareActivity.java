package com.neo.titulos;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.IsoDep;
import android.nfc.tech.MifareClassic;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.RequiresPermission;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.HashMap;

import com.neo.titulos.mifareplus.Converter;
import com.neo.titulos.mifareplus.jsonparsing.DocumentStructure;
import com.neo.titulos.model.BirthCertificateBuilder;
import com.neo.titulos.model.BirthCertificateMessage;
import com.neo.titulos.utils.Sl3Util;
import com.nxp.nfcliblite.NxpNfcLibLite;


public class ReadMifareActivity extends AppCompatActivity {

    static final String TAG = ReadMifareActivity.class.getSimpleName();
    private static Tag tagFromIntent;
    private static MifareClassic mfc;
    int errorType = 0;
    int documentType;
    String errorMsg = "";
    boolean error = false;
    ProgressDialog progressDialog;
    private NxpNfcLibLite libInstance = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read_tag);
        libInstance = NxpNfcLibLite.getInstance();
        libInstance.registerActivity(this);

        createProgressDIalog();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    private void createProgressDIalog() {
        progressDialog = new ProgressDialog(ReadMifareActivity.this);
        progressDialog.setMax(100);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setMessage("Leyendo ...espere");
    }

    private byte firstBlockInSector(int sector) {
        if (sector < 32) {
            return (byte) ((sector * 4) & 0xff);
        } else {
            return (byte) ((32 * 4 + ((sector - 32) * 16)) & 0xff);
        }
    }

    @Override
    protected void onNewIntent(final Intent intent) {
        try {
            tagFromIntent = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            String[] techs=tagFromIntent.getTechList();

            //if the tags supports IsoDep then try with SL3 else SL1 function
            if (Arrays.asList(techs).contains("android.nfc.tech.IsoDep")){ // SL3

                readSL3 op = new readSL3();
                op.execute("");

            } else { // SL1
                mfc = MifareClassic.get(tagFromIntent);
                if (mfc==null) {
                    Context context = getApplicationContext();
                    int durationMsg = Toast.LENGTH_LONG;

                    Toast toast = Toast.makeText(context, "NFC ERROR", durationMsg);
                    toast.show();
                } else {

                    LongOperation op = new LongOperation();
                    op.execute("");
                }

            }

        } catch (Exception ex){
            Log.e(TAG, "Error: " + ex.getMessage());
        }


/*        try {
            tagFromIntent = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            mfc = MifareClassic.get(tagFromIntent);
            if (mfc==null) {
                Context context = getApplicationContext();
                CharSequence text = getString(R.string.nfc_reader_error);
                int durationMsg = Toast.LENGTH_LONG;

                Toast toast = Toast.makeText(context, text, durationMsg);
                toast.show();
            } else {

                LongOperation op = new LongOperation();
                op.execute("");
            }
        }catch (Exception ex){
            Log.e(TAG, "Error: "+ex.getMessage());
        }*/

    }

    private class LongOperation extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            documentType=0;
            error=false;
            errorType=0;
            try {
                DocumentStructure.cleanData();

                mfc.connect();
                mfc.setTimeout(5000);
                Log.i(TAG, "Conectado");
                documentType = Integer.parseInt(getDocumentType(DocumentStructure.jsonDocumentType, DocumentStructure.keysDocumentType, mfc));
                Log.i(TAG, "Document type: " + documentType + "-----");

                readTagData(DocumentStructure.jsonBirthCert, DocumentStructure.memDistBirthCert, DocumentStructure.keysBirthCert, mfc);

                Vibrator vib = (Vibrator) getSystemService(VIBRATOR_SERVICE);
                long[] duration = {50, 100, 200, 300};
                vib.vibrate(duration, -1);
            }catch (NumberFormatException nfe){
                Log.e(TAG,"Sever error: "+nfe.getMessage());
                errorMsg="INVALID TAG.";
                error=true;
                errorType=1; //invalid tag


            } catch (Exception e) {
                Log.e(TAG,"Sever error: "+e.getMessage());
                errorMsg="Error while reading, try again.";
                //errorMsg=e.getMessage();
                error=true;
            }
            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                progressDialog.dismiss();
                if (error)
                {
                    if (errorType==1){
                        LayoutInflater inflater = (LayoutInflater)getSystemService(LAYOUT_INFLATER_SERVICE);
                        View layout = inflater.inflate(R.layout.invalid_dialog, (ViewGroup)findViewById(R.id.root));
                        AlertDialog.Builder adb = new AlertDialog.Builder(new ContextThemeWrapper(ReadMifareActivity.this,R.style.NeologyAlertDialogTheme));
                        adb.setView(layout);
                        adb.show();
                        /*
                        new AlertDialog.Builder(new ContextThemeWrapper(TapTagActivity.this, R.style.NeologyAlertDialogTheme))
                                .setTitle("TAG INVALIDO ")
                                .setView(R.layout.status_dialog)
                                .setNegativeButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                })
                                .setIcon(R.drawable.icon_invalid_tag)
                                .show();
                                */
                    } else {
                        Context context = getApplicationContext();
                        CharSequence text = "Error:"+errorMsg;
                        int durationMsg = Toast.LENGTH_SHORT;

                        Toast toast = Toast.makeText(context, text, durationMsg);
                        toast.show();
                    }

                }  else {
                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(i);

                }
            } catch (Exception e) {
                Log.e(TAG, "Sever error: " + e.getMessage());
                //errorMsg=e.getMessage();
                errorMsg="Error while reading, try again.";
                error=true;
            }
            // might want to change "executed" for the returned string passed
            // into onPostExecute() but that is upto you
        }

        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected void onProgressUpdate(Void... values) {}

    }
    private String getData(int sector, int block, int startByte, int size,HashMap<Integer, byte[]> keys, MifareClassic mfc) throws IOException {
        float chunkSize  = (float)size/16;
        int numberOfBlocks = (int)Math.ceil(chunkSize);
        int totalBlocks=numberOfBlocks;
        int sectorIndex=sector;
        int blockIndex=block;
        boolean auth = false;
        byte[] data;
        String resultStrGlobal="";
        // Log.i(TAG, "---------- Sector:" + sector+ " Block:" + block+" Byte:" + startByte+ " Size:"+size+ " Blocks:"+totalBlocks+" -------");
        byte[] keySector;
        if (keys.get(sectorIndex)!=null)
            keySector=keys.get(sectorIndex);
        else
            keySector=MifareClassic.KEY_DEFAULT;
        for (int i = 0; i <= totalBlocks-1; i++) {
            // Log.i(TAG, "Trying to authenticate sector:" + sectorIndex + " with the key:" + Converter.bytesToHex(keySector));
            auth = mfc.authenticateSectorWithKeyA(sectorIndex,keySector);
            if(auth) {
                byte addr = (byte) ((firstBlockInSector(sectorIndex) + blockIndex) & 0xff);
                data = mfc.readBlock(addr);
                String resultStr= Converter.hexToAscii(Converter.getHexString(data, data.length, startByte, size));
                resultStrGlobal=resultStrGlobal+ resultStr;
                if (size>=16)
                    size=size -16;
            } else {
                error=true;
                errorMsg="Security warning, one or more key are invalid.";
                break;
            }

            if (blockIndex>=2)
            {
                sectorIndex=sectorIndex+1;
                blockIndex=0;
            }else{
                blockIndex=blockIndex+1;
            }
        }
        // Log.i(TAG, "========================================");
        resultStrGlobal=resultStrGlobal.trim();
        return resultStrGlobal;
    }

    private String getDocumentType(JSONObject obj,byte[] keys, MifareClassic mfc) throws IOException, JSONException {
        boolean auth = false;
        byte[] data;
        String result="";
        auth = mfc.authenticateSectorWithKeyA(obj.getInt("sector"), keys);
        if(auth) {
            byte addr = (byte) ((firstBlockInSector(obj.getInt("sector")) + obj.getInt("block")) & 0xff);
            data = mfc.readBlock(addr);
            Log.i(TAG, data.toString());
            result= Converter.hexToAscii(Converter.getHexString(data, data.length, obj.getInt("startByte"), obj.getInt("size")));
        } else {
            error=true;
            errorMsg="Security warning, one or more key are invalid.";
        }
        return result;

    }
    @Override
    protected void onPause() {
        libInstance.stopForeGroundDispatch();
        super.onPause();
    }

    @Override
    protected void onResume() {
        libInstance.startForeGroundDispatch();
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private void readTagData(JSONObject obj,JSONArray memoryDistribution,HashMap<Integer, byte[]> keys ,MifareClassic mfc){
        try {
            for (int i = 0; i < memoryDistribution.length(); i++) {
                JSONObject jo_inside = memoryDistribution.getJSONObject(i);
                String name_value = jo_inside.getString("name");
                int sector_value = jo_inside.getInt("sector");
                int block_value = jo_inside.getInt("block");
                int startByte_value = jo_inside.getInt("startByte");
                int size_value = jo_inside.getInt("size");
                String resultTmp=getData(sector_value, block_value, startByte_value, size_value,keys, mfc);
                DocumentStructure.tagData.put(name_value, resultTmp);
                //result=result+name_value+": "+resultTmp;
            }
        } catch (Exception e) {
            //e.printStackTrace();
            Log.i(TAG, e.getMessage());
            errorMsg=e.getMessage();
            error=true;
        }

    }



    private class readSL3 extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            try {
                IsoDep isoDep = IsoDep.get(tagFromIntent);
                isoDep.setTimeout(9000);

                if (isoDep != null) {
                    // Connect to the remote NFC device
                    isoDep.connect();
                    HashMap<String, Object> session = Sl3Util.authenticate(isoDep);
                    if (session!=null) {
                        byte[] response = Sl3Util.readDataFromTag(isoDep, 1, 29, 1, session);
                        byte[] response2 = Sl3Util.readDataFromTag(isoDep, 40, 30, 2, session);
                        byte[] response3 = Sl3Util.readDataFromTag(isoDep, 80, 24, 3, session);

                        String data = new String(new String(response, Charset.forName("UTF-8"))
                                + new String(response2, Charset.forName("UTF-8"))
                                + new String(response3, Charset.forName("UTF-8")));

                        DocumentStructure.tagData.clear();
                        char docType = data.charAt(15);
                        switch (docType){
                            case '1':
                                Intent birthCertificateIntent = new Intent(getApplicationContext(), MainActivity.class);
                                BirthCertificateMessage birthCertificateMessage = new BirthCertificateBuilder(data).getBirthCertificateMessage();
                                birthCertificateIntent.putExtra("birthCertificate", birthCertificateMessage);
                                startActivity(birthCertificateIntent);
                                break;

                        }

                        Vibrator vib = (Vibrator) getSystemService(VIBRATOR_SERVICE);
                        long[] duration = {50, 100, 200, 300};
                        vib.vibrate(duration, -1);
                        error=false;
                    } else {
                        errorMsg="Can't read the tag, try again.";
                        //errorMsg=e.getMessage();
                        error=true;
                    }
                    isoDep.close();
                }


            }catch (NumberFormatException nfe){
                Log.e(TAG,"Sever error: "+nfe.getMessage());
                errorMsg="INVALID TAG.";
                error=true;
                errorType=1; //invalid tag


            } catch (Exception e) {
                Log.e(TAG,"Sever error: "+e.getMessage());
                errorMsg="Error while reading, try again.";
                //errorMsg=e.getMessage();
                error=true;
            }
            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                progressDialog.dismiss();
                if (error)
                {
                    Context context = getApplicationContext();
                    CharSequence text = "Error:"+errorMsg;
                    int durationMsg = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, text, durationMsg);
                    toast.show();
                }
            } catch (Exception e) {
                Log.e(TAG, "Sever error: " + e.getMessage());
                //errorMsg=e.getMessage();
                errorMsg="Error while reading, try again.";
                error=true;
            }
            // might want to change "executed" for the returned string passed
            // into onPostExecute() but that is upto you
        }

        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected void onProgressUpdate(Void... values) {}

    }

}
