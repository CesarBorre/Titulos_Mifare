package com.neo.titulos.subfragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.TextView;

import com.neo.titulos.R;
import com.neo.titulos.model.Certificados;
import com.neo.titulos.model.Titulos_WS_Data;
import com.neo.titulos.utils.DateUtil;
import com.neo.titulos.utils.ImageUtil;

import java.util.Date;

/**
 * Created by Cesar on 02/06/2016.
 */
public class CertificadoFrag extends Fragment {
    Certificados certificados;
    Titulos_WS_Data titulos_ws_data;

    ImageView fotoCertificado;
    TextView noCuenta;
    TextView nombre;
    TextView anioIngreso;
    TextView plantel;
    TextView carrera;
    TextView planEstudios;
    TextView obligatorios;
    TextView optativos;
    TextView credTotales;
    TextView aprobadas;
    TextView noAprobadas;
    TextView totalAsignaturas;
    TextView promedio;

    Bundle bundle;

    HorizontalScrollView table,table2;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        certificados = getArguments().getParcelable("certificados");
        bundle = getArguments();
        titulos_ws_data = (Titulos_WS_Data) getArguments().getSerializable("titulos_ws_data");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.certificado, container, false);
        fotoCertificado = (ImageView) v.findViewById(R.id.fotoCertificadoID);
        ImageUtil.setImg(titulos_ws_data.getFotoOriginal(), fotoCertificado);

        noCuenta = (TextView) v.findViewById(R.id.noCuenta);
        noCuenta.setText(String.valueOf(titulos_ws_data.getNumFOlioDocumento()));
        nombre = (TextView) v.findViewById(R.id.nombreCertificado);
        nombre.setText(titulos_ws_data.getNombre()+" "+titulos_ws_data.getApePaterno()+" "+titulos_ws_data.getApeMaterno());
        plantel = (TextView) v.findViewById(R.id.plantelID);
        plantel.setText(certificados.getPlantel());
        carrera = (TextView) v.findViewById(R.id.carreraCertID);
        carrera.setText(String.valueOf(certificados.getIntPlanEstudios()));
        planEstudios = (TextView) v.findViewById(R.id.planEstudiosID);
        planEstudios.setText(certificados.getCarrera());
        obligatorios = (TextView) v.findViewById(R.id.obligatoriosID);
        obligatorios.setText(String.valueOf(certificados.getIntCreditosObligatorios()));
        optativos = (TextView) v.findViewById(R.id.optativosID);
        optativos.setText(String.valueOf(certificados.getIntCreditosOptativos()));
        credTotales = (TextView) v.findViewById(R.id.totalesID);
        credTotales.setText(String.valueOf(certificados.getIntCreditosTotales()));
        aprobadas = (TextView) v.findViewById(R.id.aprobadas);
        aprobadas.setText(String.valueOf(certificados.getIntAsignaturasAprobadas()));
        noAprobadas = (TextView) v.findViewById(R.id.noAprobadasID);
        noAprobadas.setText(String.valueOf(certificados.getIntAsignaturasNoAprobadas()));
        totalAsignaturas = (TextView) v.findViewById(R.id.totalAsignaturasID);
        totalAsignaturas.setText(String.valueOf(certificados.getIntAsignaturasTotal()));
        promedio = (TextView) v.findViewById(R.id.promedioID);
        promedio.setText(String.valueOf(certificados.getdPromedio()));
        anioIngreso = (TextView) v.findViewById(R.id.anioIngreso);
        anioIngreso.setText(DateUtil.dateToString(new Date(certificados.getFechaIngreso()), "dd-MM-yyyy"));

        table = (HorizontalScrollView)v.findViewById(R.id.table1);
        table2 = (HorizontalScrollView)v.findViewById(R.id.table2);

        if(String.valueOf(titulos_ws_data.getNumFOlioDocumento()).equals("303016649")){
            table.setVisibility(View.VISIBLE);
            table2.setVisibility(View.GONE);
        } else {
            table.setVisibility(View.GONE);
            table2.setVisibility(View.VISIBLE);
        }
        return v;
    }
}
