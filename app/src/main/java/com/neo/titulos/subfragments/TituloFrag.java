package com.neo.titulos.subfragments;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.neo.titulos.R;
import com.neo.titulos.model.Certificados;
import com.neo.titulos.model.Titulos_WS_Data;
import com.neo.titulos.utils.ImageUtil;

import org.w3c.dom.Text;

/**
 * Created by Cesar on 02/06/2016.
 */
public class TituloFrag extends Fragment {

    TextView nombre;
    TextView nombreUni;
    TextView titulo;
    TextView secretario;
    TextView rector, el, boliviano, anioNac, dias, mes, anioTiutlo;
    ImageView fotoTitulado;
    ImageView firmaDigital;

    Titulos_WS_Data titulosData;
    Certificados certificados;
    Context c;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        titulosData = (Titulos_WS_Data) getArguments().getSerializable("titulos_ws_data");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_data, container, false);
//        this.c = container.getContext();
        init(v, getActivity());
        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.c = context;
    }

    private void init(View v, Activity a) {
        nombre = (TextView) v.findViewById(R.id.nombreID);
        nombre.setText(titulosData.getNombre() + " " + titulosData.getApePaterno() + " " + titulosData.getApeMaterno());
        setFont(nombre, 1);
        titulo = (TextView) v.findViewById(R.id.tituloID);
        titulo.setText(titulosData.getTitulo());
        setFont(titulo, 1);
        nombreUni = (TextView) v.findViewById(R.id.nombreUniID);
        nombreUni.setText(titulosData.getNombreUniversidad());
        setFont(nombreUni, 0);
        rector = (TextView) v.findViewById(R.id.rectorID);
        setFont(rector, 1);
        secretario = (TextView)v.findViewById(R.id.secretarioGralID);
        setFont(secretario, 1);
        el = (TextView)v.findViewById(R.id.elID);
        fotoTitulado = (ImageView) v.findViewById(R.id.fotoTituladoID);
        ImageUtil.setImg(titulosData.getFotoMiniatura(), fotoTitulado);
        firmaDigital = (ImageView) v.findViewById(R.id.firmaDigitalID);
        ImageUtil.setImg(titulosData.getFirmaDigital(), firmaDigital);
        secretario = (TextView) v.findViewById(R.id.secretarioGralID);
        anioNac = (TextView) v.findViewById(R.id.anioNacIDtituloTexto);
        setFont(anioNac, 1);
        boliviano = (TextView) v.findViewById(R.id.bolivianaID);
        setFont(boliviano, 1);
        dias = (TextView) v.findViewById(R.id.diasID);
        setFont(dias, 1);
        mes = (TextView) v.findViewById(R.id.mesID);
        setFont(mes, 1);
        anioTiutlo = (TextView) v.findViewById(R.id.anioTituloID);
        setFont(anioTiutlo, 1);
    }

    private void setFont(TextView t, int font) {
        Typeface type = null;
        switch (font) {
            case 0:
                type = Typeface.createFromAsset(getActivity().getAssets(), "fonts/oldlondon.ttf");
                break;
            case 1:
                type = Typeface.createFromAsset(getActivity().getAssets(), "fonts/vladimir.ttf");
                break;
        }
        t.setTypeface(type);
    }
}
