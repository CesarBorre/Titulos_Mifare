package com.neo.titulos;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.neo.titulos.adapters.PagerAdapter;
import com.neo.titulos.model.Certificados;
import com.neo.titulos.model.Titulos_WS_Data;
import com.neo.titulos.tabs.SlidingTabLayout;
import com.neo.titulos.utils.ZoomOutPagerTransformer;

public class DataActivity extends AppCompatActivity {
    Titulos_WS_Data titulosData;
    Certificados certificados;

    private ViewPager pager;
    private SlidingTabLayout mTabs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pager_layout);
        Intent intent = getIntent();
        titulosData = (Titulos_WS_Data) intent.getSerializableExtra("titulosData");
        certificados = intent.getParcelableExtra("certificados");
        slidingBar_Pager();
    }

    private void slidingBar_Pager() {
        pager = (ViewPager) findViewById(R.id.pagerID);
        pager.setAdapter(new PagerAdapter(getSupportFragmentManager(), getApplicationContext(), titulosData, certificados));
        pager.setPageTransformer(true, new ZoomOutPagerTransformer());
        mTabs = (SlidingTabLayout) findViewById(R.id.tabs);
        mTabs.setDistributeEvenly(true);
//        mTabs.setCustomTabView(R.layout.custom_tab_layout, R.id.tabText);
//        mTabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
//            @Override
//            public int getIndicatorColor(int position) {
//                return ContextCompat.getColor(getApplicationContext(), R.color.white);
//            }
//        });
        mTabs.setViewPager(pager);
    }
}
